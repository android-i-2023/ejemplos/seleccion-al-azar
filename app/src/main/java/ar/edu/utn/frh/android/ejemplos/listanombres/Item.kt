package ar.edu.utn.frh.android.ejemplos.listanombres

data class Item(
    val id: Int,
    val nombre: String
)

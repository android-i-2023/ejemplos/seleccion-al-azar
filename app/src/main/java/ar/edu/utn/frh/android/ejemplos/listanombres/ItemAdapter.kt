package ar.edu.utn.frh.android.ejemplos.listanombres

import android.app.Activity
import android.graphics.Color
import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class ItemAdapter(
    private val activity: Activity,
    private val borrar: (Int) -> Unit,
): RecyclerView.Adapter<ItemVH>() {

    private var items: List<Item> = emptyList()
    private var idSeleccionado: Int? = null

    fun cargarLista(items: List<Item>) {
        this.items = items
        notifyDataSetChanged()
    }

    fun resaltar(posicion: Int) {
        idSeleccionado = items[posicion].id
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemVH {
        Log.d("CURSO", "onCreateViewHolder")
        val vista = activity.layoutInflater.inflate(R.layout.item, parent, false)
        return ItemVH(vista)
    }

    override fun onBindViewHolder(holder: ItemVH, position: Int) {
        Log.d("CURSO", "onBindViewHolder")
        val dato = items[position]
        holder.nombreTv.text = dato.nombre
        holder.borrarBtn.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View?) {
                borrar(dato.id)
            }
        })

        if (dato.id == idSeleccionado) {
            holder.itemView.setBackgroundColor(Color.GRAY)
        } else {
            holder.itemView.setBackgroundColor(Color.TRANSPARENT)
        }
    }

    override fun getItemId(position: Int): Long {
        return items[position].id.toLong()
    }


    override fun getItemCount(): Int {
        return items.count()
    }
}
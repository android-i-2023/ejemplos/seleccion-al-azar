package ar.edu.utn.frh.android.ejemplos.listanombres

import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView.ViewHolder

class ItemVH(itemView: View): ViewHolder(itemView) {
    val nombreTv: TextView = itemView.findViewById(R.id.tvNombre)
    val borrarBtn: ImageButton = itemView.findViewById(R.id.ivBorrar)
}
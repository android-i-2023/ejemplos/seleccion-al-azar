package ar.edu.utn.frh.android.ejemplos.listanombres

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import java.util.UUID
import java.util.function.Predicate
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    private val items = mutableListOf<Item>()
    private lateinit var adapter: ItemAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = ItemAdapter(this, ::borrar)
        adapter.cargarLista(items)

        val lista = findViewById<RecyclerView>(R.id.rvLista)
        lista.adapter = adapter

        val nuevoNombreEt = findViewById<EditText>(R.id.etAgregar)

        val agregarBtn = findViewById<Button>(R.id.btAgregar)
        agregarBtn.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View?) {
                val nuevoItem = Item(
                    id = UUID.randomUUID().hashCode(),
                    nombre = nuevoNombreEt.text.toString()
                )
                items.add(nuevoItem)
                adapter.cargarLista(items)
                nuevoNombreEt.setText("")
            }
        })

        val elegirBtn = findViewById<Button>(R.id.btnElegir)
        elegirBtn.setOnClickListener {
            adapter.resaltar(seleccionarAlAzar())
        }
    }

    private fun borrar(id: Int) {
        items.removeIf(
            object: Predicate<Item> {
                override fun test(t: Item): Boolean {
                    return t.id == id
                }
            }
        )
        adapter.cargarLista(items)
    }

    private fun seleccionarAlAzar(): Int {
        return Random.nextInt(0, items.size)
    }
}







